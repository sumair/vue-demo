export const getUsersFeed = () => {
    return [
        {
            avatar: 'https://picsum.photos/200/300',
            name: 'Gabriel',
            like: 0,
            item: {
                name: 'Mountain fox notebook',
                url: 'https://picsum.photos/200/300',
                price: '€22.1'
            },
            comments: [],
            isShowComment: false
        },
        {
            avatar: 'https://picsum.photos/200/300',
            name: 'Gabriel',
            like: 0,
            item: {
                name: 'brown bear cushion',
                url: 'https://picsum.photos/200/300',
                price: '€25.5'
            },
            comments: [],
            isShowComment: false

        },
        {
            avatar: 'https://picsum.photos/200/300',
            name: 'Elysa',
            like: 0,
            item: {
                name: 'Hummingbird cushion',
                url: 'https://picsum.photos/200/300',
                price: '€112.12'
            },
            comments: [],
            isShowComment: false
        }
    ]
};