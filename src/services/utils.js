var SI_SYMBOL = ["", "K", "M", "G", "T", "P", "E"];

export const formatNumber = (number) => {

    var tier = Math.log10(number) / 3 | 0;
    if(tier === 0) return number;

    var suffix = SI_SYMBOL[tier];
    var scale = Math.pow(10, tier * 3);
    var scaled = number / scale;

    return parseFloat(scaled.toFixed(1)) + suffix;
};